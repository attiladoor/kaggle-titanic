import tensorflow as tf
from debian.debtags import output
import numpy as np
import data_transform
from datetime import datetime
from titanic_model import TitanicModell

SUBMISSION = True

INPUT_LENGTH = data_transform.VECTOR_LEN
num_classes = 2
learning_rate = 0.01
num_of_epochs = 5

if SUBMISSION:
    num_of_buckets = 1
else:
    num_of_buckets = 10

# Path for tf.summary.FileWriter and to store model checkpoints
filewriter_path = "/tmp/train_titanic"

input_tensor = tf.placeholder(tf.float32, [1, INPUT_LENGTH])
ground_truth_tensor = tf.placeholder(tf.float32, [None, num_classes])
graph = TitanicModell(input_tensor, INPUT_LENGTH)
output_tensor = graph.output

# Op for calculating the loss
with tf.name_scope("cross_ent"):
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=output_tensor, labels=ground_truth_tensor))

# Train op
with tf.name_scope("train"):
    # Create optimizer and apply gradient descent to the trainable variables
    train_op = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

# Add the loss to summary
tf.summary.scalar('cross_entropy', loss)

# Evaluation op: Accuracy of the model
with tf.name_scope("accuracy"):
    correct_pred = tf.equal(tf.argmax(output_tensor, 1), tf.argmax(ground_truth_tensor, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Add the accuracy to the summary
tf.summary.scalar('accuracy', accuracy)

# Merge all summaries together
merged_summary = tf.summary.merge_all()

# Initialize the FileWriter
writer = tf.summary.FileWriter(filewriter_path)

# Initialize an saver for store model checkpoints
saver = tf.train.Saver()

data, gt = data_transform.get_train_matrix()
accuracy = 0

# Start Tensorflow session
with tf.Session() as sess:

    print("{} Start training...".format(datetime.now()))
    print("{} Open Tensorboard at --logdir {}".format(datetime.now(), filewriter_path))

    for bucket_num in range(num_of_buckets):
        # Initialize all variables
        sess.run(tf.global_variables_initializer())

        # Add the model graph to TensorBoard
        writer.add_graph(sess.graph)

        train_data_bucket, test_data_bucket = data_transform.get_array_sub_part(data, bucket_num, num_of_buckets)
        train_gt_bucket, test_gt_bucket = data_transform.get_array_sub_part(gt, bucket_num, num_of_buckets)

        for eph in range(num_of_epochs):

            for i in range(train_data_bucket.shape[0]):

                print("bucket: %s step eph: %s step: %s" % (bucket_num, eph, i))
                sess.run(train_op, feed_dict={input_tensor: train_data_bucket[i].reshape(1, INPUT_LENGTH),
                                              ground_truth_tensor: train_gt_bucket[i].reshape(1, 2)})

                # Generate summary with the current batch of data and write to file
                s = sess.run(merged_summary, feed_dict={input_tensor: train_data_bucket[i].reshape(1, INPUT_LENGTH),
                                                        ground_truth_tensor: train_gt_bucket[i].reshape(1, 2)})
                writer.add_summary(s, eph * train_data_bucket.shape[0] + i)
        if SUBMISSION:
            f = open("data/submission.csv", 'w')
            f = open("data/submission.csv", 'a')
            f.write("PassengerId,Survived\n")

            test_data, test_gt = data_transform.get_test_matrix()
            for i in range(test_data.shape[0]):
                output_val = sess.run(output_tensor, feed_dict={input_tensor: test_data[i].reshape(1, INPUT_LENGTH)})
                line = str(int(test_gt[i][0])) + "," + str(np.argmax(output_val)) + "\n"
                f.write (line)
            f.close()

        else:
            ## evaluation
            print("{} evaluation".format(datetime.now()))
            for i in range(test_data_bucket.shape[0]):
                output_val = sess.run(output_tensor, feed_dict={input_tensor: test_data_bucket[i].reshape(1, INPUT_LENGTH),
                                                                ground_truth_tensor: test_gt_bucket[i].reshape(1, 2)})

                if np.argmax(test_gt_bucket[i]) == np.argmax(output_val):
                    accuracy += 1

            print(accuracy / data.shape[0])


