import tensorflow as tf

class TitanicModell(object):
    def __init__(self, input, input_length):
        self.INPUT_TENSOR = input
        self.INPUT_LENGTH = input_length
        self.build_modell()

    def build_modell(self):

        fc1 = fc(self.INPUT_TENSOR, self.INPUT_LENGTH, self.INPUT_LENGTH, "fc1")
        r1 = relu(fc1)
        self.output = fc(r1, self.INPUT_LENGTH, 2, "fc2")


def fc(x, num_in, num_out, name):
    with tf.variable_scope(name) as scope:
        # Create tf variables for the weights and biases
        weights = tf.get_variable('weights', shape=[num_in, num_out], trainable=True)
        biases = tf.get_variable('biases', [num_out], trainable=True)

    # Matrix multiply weights and inputs and add bias
    act = tf.nn.xw_plus_b(x, weights, biases, name=scope.name)
    return act


def relu(x):
    relu = tf.nn.relu(x)
    return relu

def lrn(x, radius, alpha, beta, name, bias=1.0):
  return tf.nn.local_response_normalization(x, depth_radius = radius, alpha = alpha,
                                            beta = beta, bias = bias, name = name)