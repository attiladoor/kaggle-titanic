# Kaggle Titanic ML challenge

Read more: https://www.kaggle.com/c/titanic

## Data

### Data Dictionary
- survival	Survival	0 = No, 1 = Yes
- pclass	Ticket class	1 = 1st, 2 = 2nd, 3 = 3rd
- sex	Sex	
- Age	Age in years	
- sibsp	# of siblings / spouses aboard the Titanic	
- parch	# of parents / children aboard the Titanic	
- ticket	Ticket number	
- fare	Passenger fare	
- cabin	Cabin number	
- embarked	Port of Embarkation	C = Cherbourg, Q = Queenstown, S = Southampton

### What input datas should we use? and Why?

When we provide data to a neural network, we should consider the relevance of data, filter unrelevants what would be just a noise for the network. In order to reveal these informations i used a python script
with MathPlotLib. First of all, does class matter? Lets see:

![alt text](documentation/survived_by_pclass.png)

Seems like yes, passengers at 3th class have worse chanse to survive. Lets see what is about the sex of passengers:

![alt text](documentation/survived_by_sex.png)

As it is on the picture, there is a significant difference between surviving chances between genders.

An interesting data parameter is "embarked" so where these pessangers board the ship. At the first sight we would say: it shouldn't matter, but let's see what is actually:

![alt text](documentation/survived_by_embarked.png)

Wait? The surviving rates are not the same, especially at those who boarded in Southampton. Maybe different type of passengers boarded at different ports? 

![alt text](documentation/classes_by_embarked.png)

Seems like in Queenstown mostly passengers of the 3th class boarded, what means, the surviving rate should be similar to the 3th class generally, but it is not the case. Maybe they were assign to other cabins.
Since these informations are missing, we cannot know, but the fact this data has something relevant for us. 

### Replacing missing data

One of the frequently missing data points is ages although it can be a key attribute of surviving. You can see some difference between the ages of all passengers and age of survivors below.

![alt text](documentation/age-number.png)

![alt text](documentation/age-of-survivors.png)

Since it can be a very useful information and at few passenger it is a missing parameter. We cannot miss this parameter empty because zero input mean zero age what is false. We better
to estimate age by other attributes. By my opinion the best is using title of passenger. 

| title	| average age   |
|-------|---------------|
| miss  |	21.77777778 |
| mr    |   32.40977444 |
| mrs	|   35.57522124 |
| master|	4.574166667 |

Later, at those passangers where the age was missing, i replaced this filed by average values respectively to to the title. The title is also an interesting topic because mainly there are Mr, Miss, Mrs, Master 
but there are many others. We should use our logic a little bit and think it over. If there is a "Don", what is this exactly? Well, he is an adult man so it must be the same as Mr.