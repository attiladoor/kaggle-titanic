#!/usr/python3
# Input data format:
# PassengerId,Survived,Pclass,Name,Sex,Age,SibSp,Parch,Ticket,Fare,Cabin,Embarked
# Out data format:
# Pclass[3] Sex[2] Title[5] Age[1] Adult[1] Children[1] Fare[1] Embarked[3] -> length = 17
import csv
import numpy as np
import os
from enum import Enum

MAX_FARE = 500

class output(Enum):
    pClassA = 0
    pClassB = 1
    pClassC = 2
    male    = 3
    female  = 4
    age     = 5
    ttl_miss= 6
    ttl_mr  = 7
    ttl_mrs = 8
    ttl_mst = 9
    ttl_dctr= 10
    embrk_S = 11
    embrk_Q = 12
    embrk_C = 13
    family_s= 14
    family_m= 15
    family_l= 16
    cabin_p = 17
    fare = 18

class input(Enum):
    id       = 0
    survived = 1
    pClass   = 2
    name     = 3
    sex      = 4
    age      = 5
    sib_ps   = 6
    parch    = 7
    ticket   = 8
    fare     = 9
    cabin    = 10
    embarked = 11

VECTOR_LEN = (output.fare.value + 1)

########constants
#average ages respectively to title
average_age = {'miss': 21.7777777,
               'mr': 32.4097744,
               'mrs': 35.5752,
               'master': 4.5741666,
               'doctor': 42}
max_age = 100
max_sibsp = 10
max_parch = 10
max_standard_price = 20
#cabin price rate of adult/children respectively to cabin class
price_rates = {'1': 1.177,
               '2': 2.05366,
               '3': 1.1196}

pclass = {'1': output.pClassA.value,
          '2': output.pClassB.value,
          '3': output.pClassC.value}

sex = {'male': output.male.value,
       'female': output.female.value}

embarked = {'S': output.embrk_S.value,
            'Q': output.embrk_Q.value,
            'C': output.embrk_C.value}

titles = {'miss': output.ttl_miss.value,
          'mr': output.ttl_mr.value,
          'mrs': output.ttl_mrs.value,
          'master': output.ttl_mst.value,
          'doctor': output.ttl_dctr.value}

family = {'small' : output.family_s.value,
          'medium': output.family_m.value,
          'large' : output.family_l.value}

family_size = {'small' : 1,
               'medium': 4}

fare_averages = {'1' : 84.15,
                 '2' : 20.98,
                 '3' : 13.67}

def get_age_and_title(line, test_offset):
    # get title
    title = get_title_from_name(line[input.name.value - test_offset])

    if line[input.age.value - test_offset] == '':
        age = average_age[title] / max_age
    else:
        age = float(line[input.age.value - test_offset])

    return age, title


def get_title_from_name(name):
    if "Mr." in name:
        return "mr"
    if "Mrs" in name:
        return "mrs"
    if "Miss" in name:
        return "miss"
    if "Master" in name:
        return "master"
    if "Dr." in name:
        return "doctor"
    if "Don." in name:
        return "mr"
    if "Rev." in name:
        return "mr"
    if "Major." in name:
        return "mr"
    if "Mme." in name:
        return "miss"
    if "Ms." in name:
        return "mrs"
    if "Sir." in name:
        return "mr"
    if "Mlle." in name:
        return "miss"
    if "Col." in name:
        return "mr"
    if "Capt." in name:
        return "mr"
    if "Countess." in name:
        return "mrs"
    if "Jonkheer." in name:
        return "mr"
    if "Dona." in name:
        return "mrs"
    return "none"


def get_starnard_ticket_price(line, age, test_offset):

    if (age > 18):  #he is an adult
        adult = (1 + int(line[input.sib_ps.value - test_offset]))
        # num of parch
        adults_from_childer = (int(line[input.sib_ps.value - test_offset]) * price_rates[line[input.pClass.value - test_offset]])
    else:           #he is a child
        # num of
        adult = int(line[input.parch.value - test_offset])
        adults_from_childer = ((1 + int(line[input.sib_ps.value - test_offset])) * price_rates[line[input.pClass.value - test_offset]])

    price = float(line[input.fare.value - test_offset]) / (adult + adults_from_childer)
    return price


def get_train_matrix():

    f = open("data/train.csv")
    csv_reader = csv.reader(f, delimiter=",")
    train_data, train_gh = get_data_matrix(csv_reader, 0)
    f.close()
    return train_data, train_gh


def get_test_matrix():

    f = open("data/test.csv")
    csv_reader = csv.reader(f, delimiter=",")
    test_data, test_gh = get_data_matrix(csv_reader, 1)
    f.close()
    return test_data, test_gh


def get_family_class(family_s):

    if family_s == family_size['small']:
        return "small"
    elif family_s <= family_size['medium']:
        return "medium"
    else:
        return "large"


def get_data_matrix(cs_reader, test_offset):

    train_matrix = np.zeros((0, VECTOR_LEN))
    ground_truth_matrix = np.zeros((0, 2))

    for line in cs_reader:
        train_vector = np.zeros(VECTOR_LEN)
        ground_truth_vector = np.zeros(2)
        try:
            ### train data
            # set pclass
            train_vector[pclass[line[input.pClass.value - test_offset]]] = 1
            # set sex
            train_vector[sex[line[input.sex.value - test_offset]]] = 1
            # set age
            age, title = get_age_and_title(line, test_offset)
            train_vector[output.age.value] = age / max_age
            # title
            train_vector[titles[title]] = 1
            # set adults and children
            family_class = get_family_class(int(line[input.sib_ps.value - test_offset]) + int(line[input.parch.value - test_offset]) + 1)
            train_vector[family[family_class]] = 1
            # set standard cabin price
            train_vector[output.cabin_p.value] = get_starnard_ticket_price(line, age, test_offset) / max_standard_price
            # embarked
            try:
                train_vector[embarked[line[input.embarked.value - test_offset]]] = 1
            except KeyError:
                pass

            #fare
            train_vector[output.fare.value] = min(float(line[input.fare.value - test_offset]) / MAX_FARE, 1)

            # concatenate results
            train_matrix = np.concatenate((train_matrix, train_vector.reshape((1, VECTOR_LEN))), axis=0)

            if not test_offset:
                ### ground truth
                ground_truth_vector[int(line[input.survived.value])] = 1
            else:
                ground_truth_vector[input.id.value] = line[input.id.value]

            ground_truth_matrix = np.concatenate((ground_truth_matrix, ground_truth_vector.reshape(1, 2)), axis=0)
        except KeyError:
            print(line)
            exit(1)

    return train_matrix, ground_truth_matrix


def get_array_sub_part(array, actual_bucket, buckets_num):
    train_data_bucket = np.zeros((0, array.shape[1]))
    test_data_bucket = np.zeros((0, array.shape[1]))
    stride = int(array.shape[0] / buckets_num)

    if buckets_num == 1:
        return array, test_data_bucket

    for i in range(array.shape[0]):
        if (i >= (stride * actual_bucket)) and (i < (stride * (actual_bucket + 1))):
            test_data_bucket = np.concatenate((test_data_bucket, array[i].reshape(1, array.shape[1])), axis=0)
        else:
            train_data_bucket = np.concatenate((train_data_bucket, array[i].reshape(1, array.shape[1])), axis=0)

    return train_data_bucket, test_data_bucket
